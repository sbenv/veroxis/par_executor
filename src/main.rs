use std::path::PathBuf;
use std::process::ExitStatus;
use std::sync::Arc;
use std::time::Duration;
use std::vec;

use anyhow::bail;
use anyhow::Result;
use tokio::fs::File;
use tokio::io::AsyncWriteExt;
use tokio::process::Command;
use tokio::sync::Semaphore;
use tokio::time::Instant;

#[derive(Debug)]
struct Config {
    concurrency: usize,
    outfile: PathBuf,
    commands: Vec<Vec<String>>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            concurrency: 8,
            outfile: PathBuf::from("out.txt"),
            commands: vec![],
        }
    }
}

impl Config {
    pub fn try_load() -> Result<Config> {
        let mut config = Self::default();
        config.load_from_env()?;
        Ok(config)
    }

    pub fn print(&self) {
        println!("Concurreny(default: 8): {}", self.concurrency);
        println!(
            "Outfile(default: \"out.txt\"): {}",
            self.outfile.to_string_lossy()
        );
        println!("Commands: {}", self.commands.len());
        for cmd in self.commands.iter() {
            println!("  {:?}", &cmd);
        }
    }

    fn load_from_env(&mut self) -> Result<()> {
        if let Ok(var) = std::env::var("PAR_EXECUTOR_CONCURRENCY") {
            match var.parse::<usize>() {
                Ok(concurrency) => self.concurrency = concurrency,
                Err(error) => bail!(error),
            };
        }
        if let Ok(var) = std::env::var("PAR_EXECUTOR_COMMANDS") {
            match serde_json::from_str::<Vec<Vec<String>>>(&var) {
                Ok(commands) => self.commands = commands,
                Err(error) => bail!(error),
            };
        }
        if let Ok(var) = std::env::var("PAR_EXECUTOR_OUTFILE") {
            self.outfile = PathBuf::from(var);
        }
        Ok(())
    }
}

#[derive(Debug)]
struct CommandResult {
    command: Vec<String>,
    duration: Duration,
    status: ExitStatus,
    stdout: String,
    stderr: String,
}

impl CommandResult {
    pub fn short_summary(&self) -> String {
        let status_prefix = match self.status.success() {
            true => "[OK]",
            false => "[ERROR]",
        };
        format!(
            "{status_prefix} Command: {:?}, Duration: {:?}, Status: {:?}",
            self.command, self.duration, self.status
        )
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let start = Instant::now();
    let config = Config::try_load()?;
    config.print();
    println!();

    let semaphore = Arc::new(Semaphore::new(config.concurrency));
    let mut command_buffer = vec![];
    for cmd in &config.commands {
        command_buffer.push(tokio::spawn(exec_cmd(semaphore.clone(), cmd.clone())));
    }

    let mut output_buffer = vec![];
    for cmd in command_buffer {
        if let Ok(Ok(output)) = cmd.await {
            output_buffer.push(output);
        }
    }

    println!("Summary:");
    for result in output_buffer.iter() {
        println!("  {}", result.short_summary());
    }

    write_logfile(&config, &output_buffer).await?;

    let finish = Instant::now();
    let elapsed = finish.duration_since(start);
    println!(
        "Total Time: {:?} ({:?})",
        elapsed,
        output_buffer
            .iter()
            .map(|cmd| cmd.duration)
            .sum::<Duration>()
    );
    for result in output_buffer.iter() {
        if !result.status.success() {
            bail!("at least one command failed");
        }
    }

    Ok(())
}

async fn exec_cmd(
    semaphore: Arc<Semaphore>,
    data: Vec<String>,
) -> Result<CommandResult, Box<dyn std::error::Error + Send + Sync>> {
    if let Some((program, args)) = data.split_first() {
        let _lock = semaphore.acquire().await;
        let start = Instant::now();
        println!("[Executing] {:?}", &data);
        let status = Command::new(program).args(args).output().await?;
        let finish = Instant::now();
        let elapsed = finish.duration_since(start);
        println!("[Finished] {:?}", &data);
        Ok(CommandResult {
            command: data,
            duration: elapsed,
            status: status.status,
            stdout: String::from_utf8_lossy(&status.stdout).to_string(),
            stderr: String::from_utf8_lossy(&status.stderr).to_string(),
        })
    } else {
        let message = format!("can't execute command: {:?}", &data);
        println!("ERROR: {}", &message);
        Err(Box::new(std::io::Error::new(
            std::io::ErrorKind::InvalidInput,
            message,
        )))
    }
}

async fn write_logfile(config: &Config, results: &[CommandResult]) -> Result<()> {
    let mut logdata = String::new();
    for result in results.iter() {
        logdata = format!(
            r##"{logdata}================================================================================
{}
stdout >>>
{}
<<< stdout
stderr >>>
{}
<<< stderr
================================================================================

"##,
            result.short_summary().trim(),
            result.stdout.trim(),
            result.stderr.trim()
        );
    }
    let mut logfile = File::create(&config.outfile).await?;
    logfile.write_all(logdata.as_bytes()).await?;
    Ok(())
}
