module.exports = {
    branches: ['*'],
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@semantic-release/changelog",
        ['@semantic-release/git', {
            assets: ['CHANGELOG.md', "Cargo.toml", "Cargo.lock"],
        }],
        ['@semantic-release/gitlab', {
            assets: [
                {
                    label: "par_executor_${nextRelease.version}_x86_64-unknown-linux-musl",
                    path: "target/x86_64-unknown-linux-musl/release/par_executor"
                },
                {
                    label: "par_executor_${nextRelease.version}_aarch64-unknown-linux-musl",
                    path: "target/aarch64-unknown-linux-musl/release/par_executor"
                },
            ],
        }]
    ],
};
