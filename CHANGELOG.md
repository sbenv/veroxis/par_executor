## [1.1.1](https://gitlab.com/sbenv/veroxis/par_executor/compare/v1.1.0...v1.1.1) (2023-01-25)


### Bug Fixes

* format ([c6db315](https://gitlab.com/sbenv/veroxis/par_executor/commit/c6db315ab16bdaecfa51f3113f14ae227965f282))
* output for all commands instead of one ([92db6f0](https://gitlab.com/sbenv/veroxis/par_executor/commit/92db6f0c80a286fadb0cd88a9b487dbb60108b3b))

# [1.1.0](https://gitlab.com/sbenv/veroxis/par_executor/compare/v1.0.4...v1.1.0) (2023-01-08)


### Features

* add aarch64 release ([112ae38](https://gitlab.com/sbenv/veroxis/par_executor/commit/112ae38b8201c1890cb6270875afa4bd125bce5a))

## [1.0.4](https://gitlab.com/sbenv/veroxis/par_executor/compare/v1.0.3...v1.0.4) (2022-06-22)


### Bug Fixes

* print whenever a command starts and finishes ([03db92a](https://gitlab.com/sbenv/veroxis/par_executor/commit/03db92a09aeb9ee291ec42781dcf34d9ec65806d))

## [1.0.3](https://gitlab.com/sbenv/veroxis/par_executor/compare/v1.0.2...v1.0.3) (2022-06-16)


### Bug Fixes

* print the duration sum of all commands ([2c0317b](https://gitlab.com/sbenv/veroxis/par_executor/commit/2c0317b1c607db18a584fd3202d10227c6b1f188))

## [1.0.2](https://gitlab.com/sbenv/veroxis/par_executor/compare/v1.0.1...v1.0.2) (2022-06-16)


### Bug Fixes

* run on a single thread ([801bbb8](https://gitlab.com/sbenv/veroxis/par_executor/commit/801bbb888ffd5b972a7048784793d1d98766ad80))

## [1.0.1](https://gitlab.com/sbenv/veroxis/par_executor/compare/v1.0.0...v1.0.1) (2022-06-16)


### Bug Fixes

* print total time ([b133b55](https://gitlab.com/sbenv/veroxis/par_executor/commit/b133b5541c53fd31848f6a9ed7559c73d816ad86))

# 1.0.0 (2022-06-16)


### Features

* initial commit ([265104d](https://gitlab.com/sbenv/veroxis/par_executor/commit/265104d82d51ba97b0c7e135346ebec425d499db))
