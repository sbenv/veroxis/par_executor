#!/bin/sh

set -eu

# build the executable
cargo build --release --target=x86_64-unknown-linux-musl

# set arguments
export PAR_EXECUTOR_CONCURRENCY="32"
export PAR_EXECUTOR_COMMANDS="$(jq '' example.json -c)"
export PAR_EXECUTOR_OUTFILE="outfile.txt"

# run it
target/x86_64-unknown-linux-musl/release/par_executor
